import re
from icecream import ic

engine_parts = []
with open("day3/input.txt") as f:
    lines = f.readlines()
    # Dectect number in lines
    for k, line in enumerate(lines):
        lines[k] = line.replace("\n", "").replace(" ", "")

    for line_number, line in enumerate(lines):
        if line_number == 0:
            previous_line = "." * len(line)
        else:
            previous_line = lines[line_number - 1]

        if line_number == len(lines) - 1:
            next_line = "." * len(line)
        else:
            next_line = lines[line_number + 1]

        matches = re.finditer(r"\D?(\d+)\D?", line)
        for m in matches:
            is_engine_part = False

            number = int(m.groups()[0])

            # search range
            number_start = line.find(str(number), m.start())
            start = number_start - 1
            number_end = number_start + len(str(number)) - 1
            end = number_end + 1

            if start == -1:
                start += 1
            if end == len(line):
                end -= 1

            string_to_check = ""
            string_to_check += previous_line[start : end + 1]
            string_to_check += line[start:number_start]
            string_to_check += line[number_end + 1 : end + 1]
            string_to_check += next_line[start : end + 1]

            # ic(number)
            # print(previous_line[start:end])
            # print(next_line[start:end])
            # print(line[start])
            # print(line[end])
            # ic(string_to_check)

            for c in string_to_check:
                if c != ".":
                    is_engine_part = True

            if is_engine_part:
                engine_parts.append(number)

ic(sum(engine_parts))
