import re
from icecream import ic
from dataclasses import dataclass


@dataclass
class EnginePart:
    idx: int
    number: int
    coord: tuple[int, int]
    symbols: list[str]
    symbol_coord: list[tuple[int, int]]


idx = 0
engine_parts: list[EnginePart] = []
with open("day3/input.txt") as f:
    lines = f.readlines()
    # Dectect number in lines
    for k, line in enumerate(lines):
        lines[k] = line.replace("\n", "").replace(" ", "")

    for line_number, line in enumerate(lines):
        if line_number == 0:
            previous_line = "." * len(line)
        else:
            previous_line = lines[line_number - 1]

        if line_number == len(lines) - 1:
            next_line = "." * len(line)
        else:
            next_line = lines[line_number + 1]

        matches = re.finditer(r"\D?(\d+)\D?", line)
        for m in matches:
            is_engine_part = False

            number = int(m.groups()[0])

            # search range
            number_start = line.find(str(number), m.start())
            start = number_start - 1
            number_end = number_start + len(str(number)) - 1
            end = number_end + 1

            if start == -1:
                start += 1
            if end == len(line):
                end -= 1

            symbols = []
            symbols_coords = []
            for k, c in enumerate(previous_line[start : end + 1]):
                if c == "*":
                    symbols.append(c)
                    symbols_coords.append((line_number - 1, start + k))

            for k, c in enumerate(line[start:number_start]):
                if c == "*":
                    symbols.append(c)
                    symbols_coords.append((line_number, start + k))

            for k, c in enumerate(line[number_end + 1 : end + 1]):
                if c == "*":
                    symbols.append(c)
                    symbols_coords.append((line_number, number_end + 1 + k))

            for k, c in enumerate(next_line[start : end + 1]):
                if c == "*":
                    symbols.append(c)
                    symbols_coords.append((line_number + 1, start + k))

            # ic(number)
            # print(previous_line[start:end])
            # print(next_line[start:end])
            # print(line[start])
            # print(line[end])
            # ic(string_to_check)
            if len(symbols) > 0:
                idx += 1
                engine_parts.append(
                    EnginePart(
                        idx,
                        number,
                        coord=(line_number, number_start),
                        symbols=symbols,
                        symbol_coord=symbols_coords,
                    )
                )

gear_ratios = []
for part_number, part in enumerate(engine_parts):
    for other_part in engine_parts[part_number + 1 :]:
        for coord in part.symbol_coord:
            for other_coord in other_part.symbol_coord:
                if coord == other_coord:
                    gear_ratio = part.number * other_part.number
                    gear_ratios.append(gear_ratio)

ic(sum(gear_ratios))
