from math import inf
import re
from icecream import ic
from dataclasses import dataclass
from tqdm import tqdm

CATEGORIES = [
    "seed",
    "soil",
    "fertilizer",
    "water",
    "light",
    "temperature",
    "humidity",
    "location",
]


def parse_seeds(line):
    values = [int(string) for string in re.findall(r" (\d+)", line)]
    starts = values[::2]
    sizes = values[1::2]
    seed_ranges = []
    for start, size in zip(starts, sizes):
        seed_ranges.append((start, size))
    return seed_ranges


def split_lines(lines: list[str]):
    blocks: list[list[str]] = []

    start_of_block = None
    for n, line in enumerate(lines):
        if "map" in line:
            start_of_block = n
            # TODO code dosnt work if miss one line at eof
        if (line == "" or n == len(lines) - 1) and start_of_block is not None:
            end_of_block = n
            blocks.append(lines[start_of_block:end_of_block])
            start_of_block = None
    return blocks


def seed_number_generator(seed_ranges: list[tuple]):
    for seed_range in seed_ranges:
        for seed_number in range(seed_range[0], seed_range[0] + seed_range[1]):
            yield seed_number


@dataclass
class Range:
    dest_start: int
    source_start: int
    size: int

    @classmethod
    def from_line(cls, line: str):
        dest_start, source_start, size = (int(n) for n in line.strip().split())
        return cls(dest_start, source_start, size)

    def is_in_source(self, number):
        return self.source_start <= number < self.source_start + self.size

    def is_in_dest(self, number):
        return self.dest_start <= number < self.dest_start + self.size

    def to_dest(self, number):
        if not self.is_in_source:
            raise ValueError

        position = number - self.source_start
        return self.dest_start + position

    def to_source(self, number):
        if not self.is_in_dest:
            raise ValueError

        position = number - self.dest_start
        return self.source_start + position


@dataclass
class Mapping:
    source_category: str
    target_category: str
    ranges: list[Range]

    @classmethod
    def from_lines(cls, lines: list[str]):
        description = lines[0]
        description = description.replace(" map:", "")
        for category in CATEGORIES:
            if description.startswith(category):
                source_category = category
            if description.endswith(category):
                target_category = category

        ranges = []
        for line in lines[1:]:
            ranges.append(Range.from_line(line))

        return cls(source_category, target_category, ranges)

    def to_dest(self, number):
        out = number
        for rang in self.ranges:
            if rang.is_in_source(number):
                out = rang.to_dest(number)
        return out

    def to_source(self, number):
        out = number
        for rang in self.ranges:
            if rang.is_in_dest(number):
                out = rang.to_source(number)
        return out


def main():
    with open("day5/input.txt") as f:
        lines = f.readlines()
        lines = [line.replace("\n", "") for line in lines]
    seed_ranges = parse_seeds(lines[0])

    blocks = split_lines(lines)

    mappings: dict[str, Mapping] = {}
    for block in blocks:
        mapping = Mapping.from_lines(block)
        mappings[mapping.source_category] = mapping

    best_location_numbers = inf
    for seed in tqdm(seed_number_generator(seed_ranges), total=1_994_747_386):
        number = seed
        for category in CATEGORIES[:-1]:
            mapping = mappings[category]
            number = mapping.to_dest(number)
        best_location_numbers = min(best_location_numbers, number)
    ic(best_location_numbers)


if __name__ == "__main__":
    main()


# 1_994_747_386
# 5:47
