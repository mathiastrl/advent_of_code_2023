import re
from icecream import ic
from dataclasses import dataclass

CATEGORIES = [
    "seed",
    "soil",
    "fertilizer",
    "water",
    "light",
    "temperature",
    "humidity",
    "location",
]


def parse_seeds(line):
    return [int(string) for string in re.findall(r" (\d+)", line)]


def split_lines(lines: list[str]):
    blocks: list[list[str]] = []

    start_of_block = None
    for n, line in enumerate(lines):
        if "map" in line:
            start_of_block = n
            # TODO code dosnt work if miss one line at eof
        if (line == "" or n == len(lines) - 1) and start_of_block is not None:
            end_of_block = n
            blocks.append(lines[start_of_block:end_of_block])
            start_of_block = None
    return blocks


@dataclass
class Range:
    dest_start: int
    source_start: int
    size: int

    @classmethod
    def from_line(cls, line: str):
        dest_start, source_start, size = (int(n) for n in line.strip().split())
        return cls(dest_start, source_start, size)

    def is_in_source(self, number):
        return self.source_start <= number < self.source_start + self.size

    def is_in_dest(self, number):
        return self.dest_start <= number < self.dest_start + self.size

    def to_dest(self, number):
        if not self.is_in_source:
            raise ValueError

        position = number - self.source_start
        return self.dest_start + position

    def to_source(self, number):
        if not self.is_in_dest:
            raise ValueError

        position = number - self.dest_start
        return self.source_start + position


@dataclass
class Mapping:
    source_category: str
    target_category: str
    ranges: list[Range]

    @classmethod
    def from_lines(cls, lines: list[str]):
        description = lines[0]
        description = description.replace(" map:", "")
        for category in CATEGORIES:
            if description.startswith(category):
                source_category = category
            if description.endswith(category):
                target_category = category

        ranges = []
        for line in lines[1:]:
            ranges.append(Range.from_line(line))

        return cls(source_category, target_category, ranges)

    def to_dest(self, number):
        out = number
        for rang in self.ranges:
            if rang.is_in_source(number):
                out = rang.to_dest(number)
        return out

    def to_source(self, number):
        out = number
        for rang in self.ranges:
            if rang.is_in_dest(number):
                out = rang.to_source(number)
        return out


def main():
    with open("day5/input.txt") as f:
        lines = f.readlines()
        lines = [line.replace("\n", "") for line in lines]
    seeds = parse_seeds(lines[0])

    blocks = split_lines(lines)

    mappings: dict[str, Mapping] = {}
    for block in blocks:
        mapping = Mapping.from_lines(block)
        mappings[mapping.source_category] = mapping

    # for n in range(110):
    #     number = mappings["soil"].to_dest(n)
    #     ic(n,number)

    location_numbers = []
    for seed in seeds:
        number = seed
        for category in CATEGORIES[:-1]:
            mapping = mappings[category]
            number = mapping.to_dest(number)
        location_numbers.append(number)
    ic(location_numbers)
    ic(min(location_numbers))


if __name__ == "__main__":
    main()
