import re
from dataclasses import dataclass


with open("day4/input.txt") as f:
    lines = f.readlines()

    scores = []
    copy_counts = [1] * len(lines)
    for line_number ,line in enumerate(lines):
        line = line.replace("\n", "")
        m = re.search(r"Card\s+(\d+):", line)
        game_number = int(m.groups()[0])
        line = line[len(m.group()) + 1 :]
        winning_str, my_numbers_str = line.split("|")
        winning_str = winning_str.strip().replace("  ", " ")
        winning_numbers = {int(n) for n in winning_str.split(" ")}

        my_numbers_str = my_numbers_str.strip().replace("  ", " ")
        my_numbers = {int(n) for n in my_numbers_str.split(" ")}
        matching_numbers = []
        for n in my_numbers:
            if n in winning_numbers:
                matching_numbers.append(n)
        for k in range(copy_counts[line_number]):

            
            for copy_to_increase in range(line_number + 1, line_number  + 1 + len(matching_numbers)):
                copy_counts[copy_to_increase] += 1 

            if not matching_numbers:
                scores.append(0)
            else:
                scores.append(2 ** (len(matching_numbers) - 1))

print("Scores", sum(scores))
print("Copy numbers", sum(copy_counts))


