from typing import Literal
import re
from dataclasses import dataclass
from icecream import ic

COLORS = ["red", "green", "blue"]


def count_color(string: str, color: str):
    number = 0
    if color in string:
        m = re.search(r"(\d+) " + color, string)
        number = m.groups()[0]
    return int(number)


@dataclass
class Game:
    id: int
    turns: list[dict[Literal["red", "green", "blue"], int]]

    @classmethod
    def from_line(cls, line: str):
        m = re.search(r"Game (\d+):", line)
        idx = int(m.groups()[0])
        line = line.replace(m.group(), "")
        turns_str = line.split(";")

        turns = []
        for turn_str in turns_str:
            turn = {}
            for color in COLORS:
                occurence_color = count_color(turn_str, color)
                turn[color] = occurence_color
            turns.append(turn)
        return cls(idx, turns)

    def is_compatible(self, game_configuration) -> bool:
        for turn in self.turns:
            for color in COLORS:
                nb_color_in_turn = turn[color]
                if nb_color_in_turn > game_configuration[color]:
                    return False
        return True

    def minimal_configuration(self):
        game_config = {"red": 0, "green": 0, "blue": 0}
        for turn in self.turns:
            for color in COLORS:
                game_config[color] = max(game_config[color], turn[color])
        return game_config

    def power(self):
        power = 1
        game_config = self.minimal_configuration()
        for number in game_config.values():
            power *= number
        return power

def parse():
    with open("day2/input.txt") as f:
        lines = f.readlines()

        games: list[Game] = []
        for line in lines:
            games.append(Game.from_line(line))
    return games

def compatible_games(games):
    GAME_CONFIGURATION = {"red": 12, "green": 13, "blue": 14}
    compatible_ids = []
    for game in games:
        if game.is_compatible(GAME_CONFIGURATION):
            compatible_ids.append(game.id)
    return sum(compatible_ids)

def sum_of_powers(games):
    powers = []
    for game in games:
        powers.append(game.power())
    return sum(powers)

if __name__ == "__main__":
    games = parse()
    print("Compatible games ",compatible_games(games))
    print(sum_of_powers(games))
    
# Time approx 45min