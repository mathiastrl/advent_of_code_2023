from icecream import ic


digits = {
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
}

def to_digit(string: str):
    d = {"one": "1", "two": "2", "three": "3", "four": "4", "five": "5",
         "six": "6", "seven": "7", "eight": "8", "nine": "9"}
    if string in d.values():
        return string
    else:
        return d[string]


def first_digit(string: str):
    f_pos = len(string)
    f_digit = ""
    for c in digits:
        pos_of_digit = string.find(c)
        if pos_of_digit != -1:
            if pos_of_digit < f_pos:
                f_pos = pos_of_digit
                f_digit = c
    return to_digit(f_digit)


def last_digit(string: str):
    l_pos = -1
    l_digit = ""
    for c in digits:
        pos_of_digit = string.rfind(c)
        if pos_of_digit != -1:
            if pos_of_digit > l_pos:
                l_pos = pos_of_digit
                l_digit = c
    return to_digit(l_digit)
    

with open("day1/input.txt") as f:
    strings = f.readlines()
    numbers = []
    for string in strings:
        ic(string)
        if string == "\n" or string == "":
            continue
        string = string.replace("\n", "")
        fst = first_digit(string)
        lst = last_digit(string)
        ic(fst, lst)
        
        ic(int(fst + lst))
        numbers.append(int(fst + lst))

ic(sum(numbers))
